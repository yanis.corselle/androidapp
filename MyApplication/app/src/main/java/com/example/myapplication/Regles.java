package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Regles extends AppCompatActivity {
    private Button btn_retour;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regles);
        btn_retour = findViewById(R.id.btn_score);
        btn_retour.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent activity_regles = new Intent(Regles.this, MainActivity.class);
                startActivity(activity_regles);
            }
        });
    }
}
