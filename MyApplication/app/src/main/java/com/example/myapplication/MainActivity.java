package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button btn_connexion, btn_inscription, btn_regles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_connexion = findViewById(R.id.btn_connexion);
        btn_connexion.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent activity_regles = new Intent(MainActivity.this, Connexion.class);
                startActivity(activity_regles);
            }
        });

        btn_inscription = findViewById(R.id.btn_inscription);
        btn_inscription.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent activity_regles = new Intent(MainActivity.this, Inscription.class);
                startActivity(activity_regles);
            }
        });

        btn_regles = findViewById(R.id.btn_score);
        btn_regles.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent activity_regles = new Intent(MainActivity.this, Regles.class);
                startActivity(activity_regles);
            }
        });
    }
}
