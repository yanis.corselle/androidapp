package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class Jeu extends AppCompatActivity {
    private Button btn_pierre, btn_papier, btn_ciseaux, btn_lezard, btn_spock, btn_score;
    int pierre = 0;
    int papier = 1;
    int ciseaux = 2;
    int lezard = 3;
    int spock = 4;
    boolean resultat;
    private TextView txt_res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jeu);
        txt_res = findViewById(R.id.txt_res);
        btn_pierre = findViewById(R.id.btn_pierre);
        btn_pierre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultat = Jeu.this.Resultat(pierre);
                if(resultat == true)
                    txt_res.setText("Gagné !");
                else
                    txt_res.setText("Perdu ...");
            }
        });
        btn_papier = findViewById(R.id.btn_papier);
        btn_papier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultat = Jeu.this.Resultat(papier);
                if(resultat == true)
                    txt_res.setText("Gagné !");
                else
                    txt_res.setText("Perdu ...");
            }
        });

        btn_ciseaux = findViewById(R.id.btn_ciseaux);
        btn_ciseaux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultat = Jeu.this.Resultat(ciseaux);
                if(resultat == true)
                    txt_res.setText("Gagné !");
                else
                    txt_res.setText("Perdu ...");
            }
        });

        btn_lezard = findViewById(R.id.btn_lezard);
        btn_lezard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultat = Jeu.this.Resultat(lezard);
                if(resultat == true)
                    txt_res.setText("Gagné !");
                else
                    txt_res.setText("Perdu ...");
            }
        });

        btn_spock = findViewById(R.id.btn_spock);
        btn_spock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultat = Jeu.this.Resultat(spock);
                if(resultat == true)
                    txt_res.setText("Gagné !");
                else
                    txt_res.setText("Perdu ...");
            }
        });

        btn_score = findViewById(R.id.btn_score);
        //btn_regles.setOnClickListener(new View.OnClickListener() {

            //public void onClick(View v) {
                //Intent  = new Intent(Jeu.this, .class);
                //startActivity();
            //}
        //});

    }



    public boolean Resultat(int choix){


        boolean resultat = false;
        Random r = new Random();
        int n = r.nextInt(5);
        if(choix == pierre)
        {
            if(n == lezard || n == ciseaux)
                return true;
            else
                return false;


        }

        else if(choix == papier)
        {
            if(n == pierre || n == spock)
                return true;
            else
                return false;

        }

        else if(choix == ciseaux)
        {
            if(n == papier || n == lezard)
                return true;
            else
                return false;

        }

        else if(choix == lezard)
        {
            if(n == spock || n == papier)
                return true;
            else
                return false;

        }

        else if(choix == spock)
        {
            if(n == ciseaux || n ==  pierre)
                return true;
            else
                return false;

        }
        return resultat;
    }
}
